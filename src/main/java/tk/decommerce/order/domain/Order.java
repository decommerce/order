package tk.decommerce.order.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "customer_order")
public record Order(@Id String id,
                    OrderStatus orderStatus,
                    Double total,
                    Date orderDate,
                    String userId,
                    @OneToMany
                    Set<LineItem> lineItems) {
}
