package tk.decommerce.order.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public record LineItem(@Id String id,
                       String product,
                       Integer quantity,
                       Double price,
                       Long inventoryId,
                       @ManyToOne Order order) {
}
