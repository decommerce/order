package tk.decommerce.order.resource;

import java.util.Date;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tk.decommerce.order.domain.LineItem;
import tk.decommerce.order.domain.OrderStatus;
import tk.decommerce.order.resource.dto.OrderDto;

@RestController
@RequestMapping("orders")
public class OrderResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderResource.class);
    
    @PostMapping
    public OrderDto creatOrder(@RequestBody OrderDto orderDto) {

        LOGGER.info("REST request to create order {}", orderDto);
        
        return orderDto;

    }

    @GetMapping
    public OrderDto getOrder() {

        OrderDto orderDto = new OrderDto("1", OrderStatus.PAID, 12.87, new Date(), "1253", new HashSet<LineItem>());
        LOGGER.info("REST request to get order {}", orderDto);
        
        return orderDto;

    }
}
