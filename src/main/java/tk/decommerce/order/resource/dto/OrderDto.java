package tk.decommerce.order.resource.dto;

import java.util.Date;
import java.util.Set;

import tk.decommerce.order.domain.LineItem;
import tk.decommerce.order.domain.OrderStatus;

public record OrderDto (String id,
                        OrderStatus orderStatus,
                        Double total,
                        Date orderDate,
                        String userId,
                        Set<LineItem> lineItems) {
}
