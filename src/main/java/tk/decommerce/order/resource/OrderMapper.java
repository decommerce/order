package tk.decommerce.order.resource;

import org.mapstruct.Mapper;

import tk.decommerce.order.domain.Order;
import tk.decommerce.order.resource.dto.OrderDto;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    
    Order dtoToEntity(OrderDto orderDto);

    OrderDto entityToDto(Order order);
}
